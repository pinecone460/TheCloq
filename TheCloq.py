#!/usr/bin/env python3
# This Python file uses the following encoding: utf-8

# (c) Copyright Hebrew year 5781 (Gregorian year 2021) pinecone460
# Some rights reserved. (See below)
# https://gitlab.com/pinecone460/TheCloq

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import sys
import os
import time
import subprocess
import json
import shutil
import tempfile


from PySide2.QtWidgets import QApplication, QWidget
from PySide2.QtCore import QFile, Slot, Qt, QUrl, QSize
from PySide2.QtUiTools import QUiLoader
from PySide2.QtMultimedia import QMediaPlayer, QMediaPlaylist
from PySide2.QtMultimediaWidgets import QVideoWidget
from PySide2.QtGui import QFontDatabase

verse = [
"You build me up",
"You break me down"
"My heart it pounds",
"Yeah you got me",

"With my hands up",
"You got me now",
"You got that sound",
"Yeah you got me",

"You build me up",
"You break me down",
"My heart it pounds",
"Yeah you got me",

"With my hands up",
"Put your hands up",
"Put your hands upupupupupup"
]

# Now the party don't start 'till I walk in...

class TheCloq(QWidget):
    def __init__(self):
        super(TheCloq, self).__init__()
        self.load_ui()

    def load_ui(self):
        try:
            os.mkdir(os.path.expanduser('~')+'/Videos')
        except FileExistsError:
            pass
        try:
            os.mkdir(os.path.expanduser('~')+'/Videos/TheCloq')
        except FileExistsError:
            pass
        self.tempdir = tempfile.mkdtemp(suffix="_TheCloq")
        os.mkdir(self.tempdir+'/Downloads')
        self.videos = []
        self.query = [None,None,None]
        loader = QUiLoader()
        QFontDatabase.addApplicationFont(os.path.join(os.path.dirname(__file__), "res/OpenSans-Regular.ttf"))
        QFontDatabase.addApplicationFont(os.path.join(os.path.dirname(__file__), "res/OpenSans-Light.ttf"))

        path = os.path.join(os.path.dirname(__file__), "form.ui")
        ui_file = QFile(path)
        ui_file.open(QFile.ReadOnly)
        self.widget = loader.load(ui_file, self)
        self.setLoading(True)
        ui_file.close()
        self.widget.videoList.setWordWrap(True)
        self.widget.videoList.verticalScrollBar().setVisible(False)
        self.player = QMediaPlayer()
        self.playlist = QMediaPlaylist()
        self.widget.videoWidget = QVideoWidget(parent=self.widget)
#        self.widget.videoWidget.setStyleSheet("background-color:white;")
        self.widget.videoWidget.move(320,91)
        self.widget.videoWidget.resize(281,500)
        self.widget.videoWidget.show()
        self.widget.playPauseButton.setVisible(False)
        self.widget.downloadButton.setVisible(False)
        self.player.setVideoOutput(self.widget.videoWidget)
        self.player.setPlaylist(self.playlist)
        self.widget.usernameLabel.setTextFormat(Qt.PlainText)
        self.widget.videoDescriptionLabel.setTextFormat(Qt.PlainText)
        self.setFixedSize(QSize(800,600))
        self.setWindowTitle("TheCloq")
        self.bindUI()
        self.setLoading(False)

    def bindUI(self):
        self.widget.searchGoButton.clicked.connect(self.search)
        self.widget.showTrendingButton.clicked.connect(self.showTrending)
        self.widget.videoList.itemClicked.connect(self.listItemSelected)
        self.widget.urlGoButton.clicked.connect(self.goToVideo)
        self.widget.playPauseButton.clicked.connect(self.playPause)
        self.widget.downloadButton.clicked.connect(self.saveVideo)

    def getVideos(self):
        self.playlist.clear()
        self.player.setPlaylist(self.playlist)
        self.player.stop()
        self.widget.usernameLabel.setText("Not playing")
        self.widget.videoDescriptionLabel.setText("Find a video using the Search or Trending tools and select it in the pane on the left, or enter a URL to view a specific video.")
        self.widget.urlField.setText("")
        self.widget.playPauseButton.setVisible(False)
        self.widget.downloadButton.setVisible(False)
        self.setLoading(True)
        assert self.query[0] in ["trend","user","hashtag"]
        if self.query[0] in ["user","hashtag"]:
            subprocess.run(['tiktok-scraper',self.query[0],self.query[1],'-n',str(self.query[2]),'-t','json','-f','search','--filepath',self.tempdir])
        elif self.query[0] == 'trend':
            subprocess.run(['tiktok-scraper',self.query[0],'-n',str(self.query[2]),'-t','json','-f','search','--filepath',self.tempdir])
        file1 = open(self.tempdir+"/search.json","r")
        self.videos = json.load(file1)
        file1.close()
        os.remove(self.tempdir+"/search.json")
        self.widget.videoList.clear()
        for i in ["@" + i['authorMeta']['name'] + " " + str(time.strftime('%m/%d/%Y %H:%M',time.localtime(i['createTime']))) for i in self.videos]:
            self.widget.videoList.addItem(i)
        self.widget.videoList.addItem("Load more...")
        self.setLoading(False)

    def search(self):
        self.playlist.clear()
        self.player.setPlaylist(self.playlist)
        self.player.stop()
        self.widget.usernameLabel.setText("Not playing")
        self.widget.videoDescriptionLabel.setText("Find a video using the Search or Trending tools and select it in the pane on the left, or enter a URL to view a specific video.")
        self.widget.urlField.setText("")
        self.setLoading(True)
        if self.widget.searchField.text() == "":
            pass
        elif self.widget.searchField.text()[0] == "@":
            self.query = ['user',self.widget.searchField.text()[1:],18]
            self.getVideos()
        elif self.widget.searchField.text()[0] == "#":
            self.query = ['hashtag',self.widget.searchField.text()[1:],18]
            self.getVideos()
        else:
            self.widget.searchField.setText("")
        self.setLoading(False)

    def showTrending(self):
        self.widget.searchField.setText("")
        self.query = ['trend',None,18]
        self.getVideos()

    def playVideo(self,videoObject,forceDownload=False,webUrl=None):
        id = videoObject['id']
        url = videoObject['videoUrl']
        if webUrl == None:
            webUrl = videoObject['webVideoUrl']
        self.playlist.clear()
        self.player.setPlaylist(self.playlist)
        self.player.stop()
        self.widget.usernameLabel.setText("")
        self.widget.videoDescriptionLabel.setText("")
        self.widget.urlField.setText(webUrl)
        self.setLoading(True)
        if (not os.path.isfile(self.tempdir + "/Downloads/" + str(id) + ".mp4")) or forceDownload:
            downloaded = True
            subprocess.run(['tiktok-scraper','video','-d',webUrl,'--filepath',self.tempdir + '/Downloads'])
        try:
            self.playlist.clear()
            self.playlist.addMedia(QUrl.fromLocalFile(self.tempdir + '/Downloads/' + str(id) + '.mp4'))
            self.playlist.setCurrentIndex(1)
            self.playlist.setPlaybackMode(QMediaPlaylist.CurrentItemInLoop)
            self.player.setPlaylist(self.playlist)
            self.player.play()
            self.videoPath = self.tempdir + '/Downloads/' + str(id) + '.mp4'
        except Exception as e:
            if downloaded:
                print("An error occurred. TheCloq may have tried to resolve this by forcing the video download, or it may not have if it was already downloading this video. In any case, it didn't work. We're throwing the error -- catch!")
                raise e
            else:
                print("An error occurred -- but this may be because of a faulty video file (we thought we had it cached, but the cached one might be faulty). We'll try forcing the download to see if it works with a clean video.")
                self.playVideo(id,url,forceDownload=True)
                return
        self.widget.usernameLabel.setText("@"+videoObject['authorMeta']['name'])
        self.widget.videoDescriptionLabel.setText(str(time.strftime('%m/%d/%Y %H:%M',time.localtime(videoObject['createTime'])))+"\n\n"+videoObject['text'])
        self.widget.playPauseButton.setVisible(True)
        self.widget.downloadButton.setVisible(True)
        self.setLoading(False)

    def setLoading(self,loading):
        if loading:
            self.loading = True
            self.widget.loadingLabel.setText("Loading...")
            self.repaint()
        else:
            self.loading = False
            self.widget.loadingLabel.setText("")
            self.repaint()

    def listItemSelected(self):
        if self.widget.videoList.currentRow() + 1 > len(self.videos):
            self.query[2] += 9
            self.getVideos()
        else:
            self.playVideo(self.videos[self.widget.videoList.currentRow()])

    def goToVideo(self):
        self.widget.videoList.clear()
        self.widget.searchField.setText("")
        self.playlist.clear()
        self.player.setPlaylist(self.playlist)
        self.player.stop()
        self.widget.usernameLabel.setText("")
        self.widget.videoDescriptionLabel.setText("")
        self.widget.playPauseButton.setVisible(False)
        self.widget.downloadButton.setVisible(False)
        self.setLoading(True)

        self.query = ['video',self.widget.urlField.text(),None]
        try:
            shutil.rmtree(self.tempdir + "/videoData")
        except FileNotFoundError:
            pass
        os.mkdir(self.tempdir + "/videoData")
        subprocess.run(['tiktok-scraper',self.query[0],self.query[1],'-t','json','--filepath',self.tempdir + '/videoData'])
        file1paths = os.listdir(self.tempdir + "/videoData")
        for i in file1paths:
            if i.endswith('.json'):
                file1path = i
        assert file1path
        file1 = open(self.tempdir + "/videoData/"+file1path,"r")
        self.videos = json.load(file1)
        file1.close()
        shutil.rmtree(self.tempdir + "/videoData")
        print(self.videos[0])
        self.playVideo(self.videos[0],webUrl=self.query[1])

    def playPause(self):
        if self.player.state() == QMediaPlayer.PlayingState:
            self.player.pause()
        elif self.player.state() == QMediaPlayer.PausedState:
            self.player.play()

    def saveVideo(self):
        shutil.copy(self.videoPath,os.path.expanduser('~')+'/Videos/TheCloq/')

    def cleanUp(self):
        try:
            shutil.rmtree(self.tempdir)
        except FileNotFoundError:
            pass

if __name__ == "__main__":
    app = QApplication([])
    widget = TheCloq()
    app.aboutToQuit.connect(widget.cleanUp)
    widget.show()
    sys.exit(app.exec_())
