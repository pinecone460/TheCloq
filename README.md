# TheCloq
#### but the party don't stop, nnno
#### ohuohuah ohh, heyyy! ohuohuah ohh!

TheCloq is a free/libre/open source GUI for browsing public videos on TikTok. It runs on (GNU/)Linux and may run on other systems, especially Unix-like ones -- it has been tested on Mac OS X (on which I insist this name; see below), but not as extensively.

Download/Install/Run:
---
#### Download TheCloq

Clone the git repo and cd in:

```
git clone https://gitlab.com/pinecone460/TheCloq
cd TheCloq
```

#### Install dependencies (Debian/Ubuntu):

First make sure you have [NodeJS](https://nodejs.org/) installed (version 10 or later), then run:

```
sudo apt install python3 libpyside2-dev
sudo npm i -g tiktok-scraper
```

#### Install dependencies (Mac OS X, which I refuse to call macOS):

First make sure you have [NodeJS](https://nodejs.org/) (version 10 or later), [Homebrew](https://brew.sh), and the [Mac Command Line Developer Tools](https://developer.apple.com/downloads/) (running `python3` should install them) installed, then run:

```
brew install pyside
sudo npm i -g tiktok-scraper
```

#### Install dependencies on Windows:

Wow, you really picked the hard one.

I must confess to you.

I know nothing about Windows.

Bear with me for a moment.

Okay, I haven't tried this, but I think you install Node, Python 3, and Qt 5?

And then use `npm` to install `tiktok-scraper` and `pip` to install `PySide2`?

If you've read this far, then your guess is probably as good as mine.

#### Run TheCloq

Launch TheCloq by running `python3 TheCloq.py` or simply `./TheCloq.py`

Licensing
---
TheCloq is available under [version 2 of the Mozilla Public License](https://www.mozilla.org/en-US/MPL/2.0/). It depends on [PySide2](https://pypi.org/project/PySide2/) and [Qt](https://www.qt.io/), which are used under the [GNU Lesser General Public License, Version 3](https://www.gnu.org/licenses/lgpl-3.0.en.html), on [tiktok-scraper](https://www.npmjs.com/package/tiktok-scraper), which is used under the [Expat ("MIT") License](https://www.debian.org/legal/licenses/mit), and on the Open Sans font family, which is used under the [Apache License](https://www.apache.org/licenses/LICENSE-2.0). A copy of the MPLv2 is available in this repo as `LICENSE`, and the other three licenses are available in the directory `otherLicenses`, but they do not apply to TheCloq itself.

Other notes
---
Videos saved with TheCloq are stored in the `~/Videos/TheCloq` directory. This is created if it doesn't already exist.
